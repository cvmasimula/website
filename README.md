# website

My team created a tutoring website with a "close to home" perspective when it comes to the relationship between the tutor and the student. Students new to the system can search for tutors by name or by the subject they teach.